#lcc path in path variable shell
CC = lcc -Wa-l -Wl-m -Wl-j

BINS = 	verlies.gb

all:	$(BINS)

clean:
	rm -rf obj/* bin/*


## Daten

#hud
hudgui.o:			daten/hud/hudgui.c
				$(CC) -Wf-ba8 -c -o obj/hudgui.o daten/hud/hudgui.c
rahmen.o:			gfx/rahmen.c
				$(CC) -Wf-bo10 -c -o obj/rahmen.o gfx/rahmen.c

#karten
maps.o:				daten/karten/maps.c
				$(CC) -Wf-bo3 -c -o obj/maps.o daten/karten/maps.c

nomap.o:			daten/karten/nomap.c
				$(CC) -Wf-bo3 -c -o obj/nomap.o daten/karten/nomap.c

#lvl
lvldatfelsengrund.o:		daten/lvl/lvldatfelsengrund.c
				$(CC) -Wf-bo5 -c -o obj/lvldatfelsengrund.o daten/lvl/lvldatfelsengrund.c

lvldatstadtgefaengnis.o:	daten/lvl/lvldatstadtgefaengnis.c
				$(CC) -Wf-bo5 -c -o obj/lvldatstadtgefaengnis.o daten/lvl/lvldatstadtgefaengnis.c

lvldatwiesen.o:			daten/lvl/lvldatwiesen.c
				$(CC) -Wf-bo5 -c -o obj/lvldatwiesen.o daten/lvl/lvldatwiesen.c

lvldatogerhoehlen.o:		daten/lvl/lvldatogerhoehlen.c
				$(CC) -Wf-bo6 -c -o obj/lvldatogerhoehlen.o daten/lvl/lvldatogerhoehlen.c

lvldatdorfseefeen.o:		daten/lvl/lvldatdorfseefeen.c
				$(CC) -Wf-bo6 -c -o obj/lvldatdorfseefeen.o daten/lvl/lvldatdorfseefeen.c

lvldatfeensee.o:		daten/lvl/lvldatfeensee.c
				$(CC) -Wf-bo6 -c -o obj/lvldatfeensee.o daten/lvl/lvldatfeensee.c

lvldateichenwald.o:		daten/lvl/lvldateichenwald.c
				$(CC) -Wf-bo18 -c -o obj/lvldateichenwald.o daten/lvl/lvldateichenwald.c

lvldatalterbaum.o:		daten/lvl/lvldatalterbaum.c
				$(CC) -Wf-bo18 -c -o obj/lvldatalterbaum.o daten/lvl/lvldatalterbaum.c

lvldatgebirgspfad.o:		daten/lvl/lvldatgebirgspfad.c
				$(CC) -Wf-bo18 -c -o obj/lvldatgebirgspfad.o daten/lvl/lvldatgebirgspfad.c

lvldatzwergenheim.o:		daten/lvl/lvldatzwergenheim.c
				$(CC) -Wf-bo18 -c -o obj/lvldatzwergenheim.o daten/lvl/lvldatzwergenheim.c

lvldatrotgebirge.o:		daten/lvl/lvldatrotgebirge.c
				$(CC) -Wf-bo19 -c -o obj/lvldatrotgebirge.o daten/lvl/lvldatrotgebirge.c

lvldatminen-1.o:		daten/lvl/lvldatminen-1.c
				$(CC) -Wf-bo19 -c -o obj/lvldatminen-1.o daten/lvl/lvldatminen-1.c

lvldatminen-2.o:		daten/lvl/lvldatminen-2.c
				$(CC) -Wf-bo19 -c -o obj/lvldatminen-2.o daten/lvl/lvldatminen-2.c

lvldatminen-3.o:		daten/lvl/lvldatminen-3.c
				$(CC) -Wf-bo19 -c -o obj/lvldatminen-3.o daten/lvl/lvldatminen-3.c 

lvldatgipfelpfad.o:		daten/lvl/lvldatgipfelpfad.c
				$(CC) -Wf-bo19 -c -o obj/lvldatgipfelpfad.o daten/lvl/lvldatgipfelpfad.c

lvldattempel-tg.o:		daten/lvl/lvldattempel-tg.c
				$(CC) -Wf-bo19 -c -o obj/lvldattempel-tg.o daten/lvl/lvldattempel-tg.c

lvldattempel-kg.o:		daten/lvl/lvldattempel-kg.c
				$(CC) -Wf-bo20 -c -o obj/lvldattempel-kg.o daten/lvl/lvldattempel-kg.c

lvldattempel-E1.o:		daten/lvl/lvldattempel-E1.c
				$(CC) -Wf-bo20 -c -o obj/lvldattempel-E1.o daten/lvl/lvldattempel-E1.c

lvldattempel-E2.o:		daten/lvl/lvldattempel-E2.c
				$(CC) -Wf-bo20 -c -o obj/lvldattempel-E2.o daten/lvl/lvldattempel-E2.c

v_lvl400.o:			daten/lvl/v_lvl400.c
				$(CC) -Wf-bo20 -c -o obj/v_lvl400.o daten/lvl/v_lvl400.c	

#other
gameover.o:			daten/other/gameover.c
				$(CC) -Wf-bo3 -c -o obj/gameover.o daten/other/gameover.c

titel.o:			daten/other/titel.c
				$(CC) -Wf-bo3 -c -o obj/titel.o daten/other/titel.c

kredits.o:			daten/other/kredits.c
				$(CC) -Wf-bo3 -c -o obj/kredits.o daten/other/kredits.c

anleitung.o:			daten/other/anleitung.c
				$(CC) -Wf-bo3 -c -o obj/anleitung.o daten/other/anleitung.c

itembeschreibung.o:		daten/other/itembeschreibung.c
				$(CC) -Wf-bo3 -c -o obj/itembeschreibung.o daten/other/itembeschreibung.c

statusanzeigebeschreibung.o:	daten/other/statusanzeigebeschreibung.c
				$(CC) -Wf-bo3 -c -o obj/statusanzeigebeschreibung.o daten/other/statusanzeigebeschreibung.c

#spriteset
v_spriteset_1.o:		daten/spritesets/v_spriteset_1.c
				$(CC) -Wf-bo2 -c -o obj/v_spriteset_1.o daten/spritesets/v_spriteset_1.c

v_spriteset_2.o:		daten/spritesets/v_spriteset_2.c
				$(CC) -Wf-bo2 -c -o obj/v_spriteset_2.o daten/spritesets/v_spriteset_2.c

spriteset_player_1_m.o:		daten/spritesets/spriteset_player_1_m.c
				$(CC) -Wf-bo2 -c -o obj/spriteset_player_1_m.o daten/spritesets/spriteset_player_1_m.c

spriteset_player_3_m.o:		daten/spritesets/spriteset_player_3_m.c
				$(CC) -Wf-bo2 -c -o obj/spriteset_player_3_m.o daten/spritesets/spriteset_player_3_m.c

tilesets.o:			daten/tilesets/tilesets.c
				$(CC) -Wf-bo2 -c -o obj/tilesets.o daten/tilesets/tilesets.c
#Texte locations
locationstxt.o:			daten/txt/locations/locations.c
				$(CC) -Wf-bo3 -c -o obj/locationstxt.o daten/txt/locations/locations.c

#Texte von Npcs und Gegnern
haendler.o:			daten/txt/npcs/haendler.c
				$(CC) -Wf-bo10 -c -o obj/haendler.o daten/txt/npcs/haendler.c

txtboss.o:			daten/txt/npcs/boss.c
				$(CC) -Wf-bo10 -c -o obj/txtboss.o daten/txt/npcs/boss.c

heilertxt.o:			daten/txt/npcs/heilertxt.c
				$(CC) -Wf-bo10 -c -o obj/heilertxt.o daten/txt/npcs/heilertxt.c

hugo.o:				daten/txt/npcs/hugo.c
				$(CC) -Wf-bo10 -c -o obj/hugo.o daten/txt/npcs/hugo.c

phobetxt.o:			daten/txt/npcs/phobetxt.c
				$(CC) -Wf-bo10 -c -o obj/phobetxt.o daten/txt/npcs/phobetxt.c

sara.o:				daten/txt/npcs/sara.c
				$(CC) -Wf-bo10 -c -o obj/sara.o daten/txt/npcs/sara.c

handelnichtmoeglich.o:		daten/txt/other/handelnichtmoeglich.c
				$(CC) -Wf-bo10 -c -o obj/handelnichtmoeglich.o daten/txt/other/handelnichtmoeglich.c

feenaeltestetxt.o:		daten/txt/npcs/feenaeltestetxt.c
				$(CC) -Wf-bo10 -c -o obj/feenaeltestetxt.o daten/txt/npcs/feenaeltestetxt.c

ranartxt.o:			daten/txt/npcs/ranartxt.c
				$(CC) -Wf-bo10 -c -o obj/ranartxt.o daten/txt/npcs/ranartxt.c

vanyra.o:			daten/txt/npcs/vanyra.c
				$(CC) -Wf-bo10 -c -o obj/vanyra.o daten/txt/npcs/vanyra.c

othertxt.o:			daten/txt/npcs/others.c
				$(CC) -Wf-bo10 -c -o obj/othertxt.o daten/txt/npcs/others.c

hueterin.o:			daten/txt/npcs/hueterin.c
				$(CC) -Wf-bo10 -c -o obj/hueterin.o daten/txt/npcs/hueterin.c

xaver.o:			daten/txt/npcs/xaver.c
				$(CC) -Wf-bo10 -c -o obj/xaver.o daten/txt/npcs/xaver.c

# Texte Portale

portaltxt.o:			daten/txt/portale/portal.c
				$(CC) -Wf-bo8 -c -o obj/portaltxt.o daten/txt/portale/portal.c

portaletxt.o:			daten/txt/portale/portaletxt.c
				$(CC) -Wf-bo10 -c -o obj/portaletxt.o daten/txt/portale/portaletxt.c

#Texte Schilder
schildertxt.o:			daten/txt/schilder/schildertxt.c
				$(CC) -Wf-bo10 -c -o obj/schildertxt.o daten/txt/schilder/schildertxt.c

#andere Texte
schluessel.o:			daten/txt/schilder/schluessel.c
				$(CC) -Wf-bo10 -c -o obj/schluessel.o daten/txt/schilder/schluessel.c

oelloch.o:			daten/txt/other/oelloch.c
				$(CC) -Wf-bo10 -c -o obj/oelloch.o daten/txt/other/oelloch.c

oelloch1.o:			daten/txt/other/oelloch1.c
				$(CC) -Wf-bo10 -c -o obj/oelloch1.o daten/txt/other/oelloch1.c

oelloch2.o:			daten/txt/other/oelloch2.c
				$(CC) -Wf-bo10 -c -o obj/oelloch2.o daten/txt/other/oelloch2.c

alpha.o:			daten/txt/other/alpha.c
				$(CC) -Wf-bo10 -c -o obj/alpha.o daten/txt/other/alpha.c

intro.o:			daten/txt/other/intro.c
				$(CC) -Wf-bo10 -c -o obj/intro.o daten/txt/other/intro.c

#Texte Truhen
truhentxt.o:			daten/txt/truhen/truhentxt.c
				$(CC) -Wf-bo10 -c -o obj/truhentxt.o daten/txt/truhen/truhentxt.c 

## Daten Ende


#lvlablauf
felsengrund.o:			felsengrund.c
				$(CC) -Wf-bo15 -c -o obj/felsengrund.o felsengrund.c

stadtgefaengnis.o:		stadtgefaengnis.c
				$(CC) -Wf-bo16 -c -o obj/stadtgefaengnis.o stadtgefaengnis.c

ogerhoehlen.o:			ogerhoehlen.c
				$(CC) -Wf-bo16 -c -o obj/ogerhoehlen.o ogerhoehlen.c

wiesen.o:				wiesen.c
				$(CC) -Wf-bo16 -c -o obj/wiesen.o wiesen.c

feensee.o:			feensee.c
				$(CC) -Wf-bo16 -c -o obj/feensee.o feensee.c

eichenwald.o:			eichenwald.c
				$(CC) -Wf-bo16 -c -o obj/eichenwald.o eichenwald.c

eichenwald2.o:			eichenwald2.c
				$(CC) -Wf-bo17 -c -o obj/eichenwald2.o eichenwald2.c

alte-baum.o:			alte-baum.c
				$(CC) -Wf-bo17 -c -o obj/alte-baum.o alte-baum.c

gebirgspfad.o:			gebirgspfad.c
				$(CC) -Wf-bo17 -c -o obj/gebirgspfad.o gebirgspfad.c

zwergenheim.o:			zwergenheim.c
				$(CC) -Wf-bo17 -c -o obj/zwergenheim.o zwergenheim.c

rotgebirge.o:			rotgebirge.c
				$(CC) -Wf-bo17 -c -o obj/rotgebirge.o rotgebirge.c

minen.o:			minen.c
				$(CC) -Wf-bo17 -c -o obj/minen.o minen.c

minen2.o:			minen2.c
				$(CC) -Wf-bo17 -c -o obj/minen2.o minen2.c

minen3.o:			minen3.c
				$(CC) -Wf-bo17 -c -o obj/minen3.o minen3.c

gipfelpfad.o:			gipfelpfad.c
				$(CC) -Wf-bo17 -c -o obj/gipfelpfad.o gipfelpfad.c

tempel.o:				tempel.c
				$(CC) -Wf-bo17 -c -o obj/tempel.o tempel.c

tempel-kg.o:			tempel-kg.c
				$(CC) -Wf-bo17 -c -o obj/tempel-kg.o tempel-kg.c

tempel-e1.o:			tempel-e1.c
				$(CC) -Wf-bo17 -c -o obj/tempel-e1.o tempel-e1.c

tempel-e2.o:			tempel-e2.c
				$(CC) -Wf-bo17 -c -o obj/tempel-e2.o tempel-e2.c

#main
verlies.o:			verlies.c
				$(CC) -c -o obj/verlies.o verlies.c

globals.o:				globals.c
				$(CC) -c -o obj/globals.o globals.c

engine.o:				engine.c
				$(CC) -c -o obj/engine.o engine.c

file.o:				file.c
				$(CC) -Wf-ba1 -Wf-bo1 -c -o obj/file.o file.c

locations.o:			locations.c
				$(CC) -Wf-bo3 -c -o obj/locations.o locations.c

map.o:				map.c
				$(CC) -Wf-bo3 -c -o obj/map.o map.c

other.o:			other.c
				$(CC) -Wf-bo3 -c -o obj/other.o other.c

init.o:				init.c
				$(CC) -Wf-bo4 -c -o obj/init.o init.c

truhen.o:			truhen.c
				$(CC) -Wf-bo7 -c -o obj/truhen.o truhen.c

hud.o:				hud.c
				$(CC) -Wf-bo8 -c -o obj/hud.o hud.c

portale.o:			portale.c
				$(CC) -Wf-bo8 -c -o obj/portale.o portale.c

infoscreen.o:			infoscreen.c
				$(CC) -Wf-bo8 -c -o obj/infoscreen.o infoscreen.c

player.o:			player.c
				$(CC) -Wf-bo9 -c -o obj/player.o player.c

tiledat.o:			tiledat.c
				$(CC) -Wf-bo9 -c -o obj/tiledat.o tiledat.c

text.o:				text.c
				$(CC) -Wf-bo10 -c -o obj/text.o text.c

lvlstatus.o:			lvlstatus.c
				$(CC) -Wf-bo11 -c -o obj/lvlstatus.o lvlstatus.c

tiledatg.o:			tiledatg.c
				$(CC) -Wf-bo11 -c -o obj/tiledatg.o tiledatg.c

items.o:			items.c
				$(CC) -Wf-bo12 -c -o obj/items.o items.c

npc.o:				npc.c
				$(CC) -Wf-bo12 -c -o obj/npc.o npc.c

schilder.o:			schilder.c
				$(CC) -Wf-bo12 -c -o obj/schilder.o schilder.c

runen.o:			runen.c
				$(CC) -Wf-bo12 -c -o obj/runen.o runen.c

lvlgegner.o:			lvlgegner.c
				$(CC) -Wf-bo13 -c -o obj/lvlgegner.o lvlgegner.c

boss.o:				boss.c
				$(CC) -Wf-bo14 -c -o obj/boss.o boss.c

gegner.o:			gegner.c
				$(CC) -Wf-bo14 -c -o obj/gegner.o gegner.c

umgebung.o:			umgebung.c
				$(CC) -Wf-bo21 -c -o obj/umgebung.o umgebung.c

endkampf.o:			endkampf.c
				$(CC) -Wf-bo21 -c -o obj/endkampf.o endkampf.c

verlies.gb:	hudgui.o rahmen.o maps.o nomap.o lvldatfelsengrund.o lvldatstadtgefaengnis.o lvldatwiesen.o lvldatogerhoehlen.o lvldatdorfseefeen.o lvldatfeensee.o lvldateichenwald.o lvldatalterbaum.o lvldatgebirgspfad.o lvldatzwergenheim.o lvldatrotgebirge.o lvldatminen-1.o lvldatminen-2.o lvldatminen-3.o lvldatgipfelpfad.o lvldattempel-tg.o lvldattempel-kg.o lvldattempel-E1.o lvldattempel-E2.o v_lvl400.o gameover.o titel.o kredits.o anleitung.o itembeschreibung.o statusanzeigebeschreibung.o v_spriteset_1.o v_spriteset_2.o spriteset_player_1_m.o spriteset_player_3_m.o tilesets.o locationstxt.o haendler.o txtboss.o heilertxt.o hugo.o phobetxt.o sara.o handelnichtmoeglich.o feenaeltestetxt.o ranartxt.o vanyra.o othertxt.o hueterin.o xaver.o portaltxt.o portaletxt.o schildertxt.o schluessel.o oelloch.o oelloch1.o oelloch2.o alpha.o intro.o truhentxt.o felsengrund.o stadtgefaengnis.o ogerhoehlen.o wiesen.o feensee.o eichenwald.o eichenwald2.o alte-baum.o gebirgspfad.o zwergenheim.o rotgebirge.o minen.o minen2.o minen3.o gipfelpfad.o tempel.o tempel-kg.o tempel-e1.o tempel-e2.o verlies.o globals.o engine.o file.o locations.o map.o other.o init.o truhen.o hud.o portale.o infoscreen.o infoscreen.o player.o tiledat.o text.o lvlstatus.o tiledatg.o items.o npc.o schilder.o runen.o lvlgegner.o boss.o gegner.o umgebung.o endkampf.o

		$(CC) -Wl-yt0x019 -Wl-yo32 -Wl-ya1B -Wm-yn"VERLIES" -o obj/verlies.gb obj/hudgui.o obj/rahmen.o obj/maps.o obj/nomap.o obj/lvldatfelsengrund.o obj/lvldatstadtgefaengnis.o obj/lvldatwiesen.o obj/lvldatogerhoehlen.o obj/lvldatdorfseefeen.o obj/lvldatfeensee.o obj/lvldateichenwald.o obj/lvldatalterbaum.o obj/lvldatgebirgspfad.o obj/lvldatzwergenheim.o obj/lvldatrotgebirge.o obj/lvldatminen-1.o obj/lvldatminen-2.o obj/lvldatminen-3.o obj/lvldatgipfelpfad.o obj/lvldattempel-tg.o obj/lvldattempel-kg.o obj/lvldattempel-E1.o obj/lvldattempel-E2.o obj/v_lvl400.o obj/gameover.o obj/titel.o obj/kredits.o obj/anleitung.o obj/itembeschreibung.o obj/statusanzeigebeschreibung.o obj/v_spriteset_1.o obj/v_spriteset_2.o obj/spriteset_player_1_m.o obj/spriteset_player_3_m.o obj/tilesets.o obj/locationstxt.o obj/haendler.o obj/txtboss.o obj/heilertxt.o obj/hugo.o obj/phobetxt.o obj/sara.o obj/handelnichtmoeglich.o obj/feenaeltestetxt.o obj/ranartxt.o obj/vanyra.o obj/othertxt.o obj/hueterin.o obj/xaver.o obj/portaltxt.o obj/portaletxt.o obj/schildertxt.o obj/schluessel.o obj/oelloch.o obj/oelloch1.o obj/oelloch2.o obj/alpha.o obj/intro.o obj/truhentxt.o obj/felsengrund.o obj/stadtgefaengnis.o obj/ogerhoehlen.o obj/wiesen.o obj/feensee.o obj/eichenwald.o obj/eichenwald2.o obj/alte-baum.o obj/gebirgspfad.o obj/zwergenheim.o obj/rotgebirge.o obj/minen.o obj/minen2.o obj/minen3.o obj/gipfelpfad.o obj/tempel.o obj/tempel-kg.o obj/tempel-e1.o obj/tempel-e2.o obj/verlies.o obj/globals.o obj/engine.o obj/file.o obj/locations.o obj/map.o obj/other.o obj/init.o obj/truhen.o obj/hud.o obj/portale.o obj/infoscreen.o obj/infoscreen.o obj/player.o obj/tiledat.o obj/text.o obj/lvlstatus.o obj/tiledatg.o obj/items.o obj/npc.o obj/schilder.o obj/runen.o obj/lvlgegner.o obj/boss.o obj/gegner.o obj/umgebung.o obj/endkampf.o
		cp -rf obj/verlies.gb bin/
