//   Verlies - ein Adventure im Retrodesign
//
//   Copyright (C) 2018-2022 Heiko Wolf
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License As published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
//   GNU General Public License For more details.
//
//   You should have received a copy of the GNU General Public License along
//   With this program; if not, write to the Free Software Foundation, Inc.,
//   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//   Kontakt: projekte@kabelmail.net

#ifndef _HAENDLERTXT_H_
#define _HAENDLERTXT_H_

extern const unsigned char ausdauerverkaeuferin1[72];
extern const unsigned char ausdauerverkaeuferin2[72];
extern const unsigned char heiltrankverkaeuferin1[72];
extern const unsigned char heiltrankverkaeuferin2[72];
extern const unsigned char kraeuterhaendler1[72];
extern const unsigned char kraeuterhaendler2[72];
extern const unsigned char provianthaendler1[72];
extern const unsigned char provianthaendler2[72];
extern const unsigned char zauberstaubhaendler1[72];
extern const unsigned char zauberstaubhaendler2[72];
extern const unsigned char fiona1[72];
extern const unsigned char fiona2[72];
extern const unsigned char fiona3[72];
extern const unsigned char max1[72];
extern const unsigned char max2[72];
extern const unsigned char max3[72];
extern const unsigned char zylratxt1[72];

#endif
