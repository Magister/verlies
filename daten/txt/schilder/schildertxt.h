//   Verlies - ein Adventure im Retrodesign
//
//   Copyright (C) 2018-2023 Heiko Wolf
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License As published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
//   GNU General Public License For more details.
//
//   You should have received a copy of the GNU General Public License along
//   With this program; if not, write to the Free Software Foundation, Inc.,
//   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//   Kontakt: projekte@kabelmail.net

#ifndef _SCHILDTXT_H_
#define _SCHILDTXT_H_

extern const unsigned char schildtxt10[72];
extern const unsigned char schildtxt11[72];
extern const unsigned char schildtxt12[72];
extern const unsigned char schildtxt1b[72];
extern const unsigned char schildtxt1c[72];
extern const unsigned char schildtxt1d[72];
extern const unsigned char schildtxt1e[72];
extern const unsigned char schildtxt1[72];
extern const unsigned char schildtxt2[72];
extern const unsigned char schildtxt3[72];
extern const unsigned char schildtxt4[72];
extern const unsigned char schildtxt5[72];
extern const unsigned char schildtxt6[72];
extern const unsigned char schildtxt7[72];
extern const unsigned char schildtxt8[72];
extern const unsigned char schildtxt9[72];
extern const unsigned char schildtxt13[72];
extern const unsigned char schildtxt14[72];
extern const unsigned char schildtxt15[72];
extern const unsigned char schildtxt16[72];
extern const unsigned char schildtxt17[72];
extern const unsigned char schildtxt18[72];
extern const unsigned char schildtxt19[72];
extern const unsigned char schildtxt20[72];
extern const unsigned char schildtxt21[72];
extern const unsigned char schildtxt22[72];
extern const unsigned char schildtxt23[72];
extern const unsigned char schildtxt24[72];
extern const unsigned char schildtxt25[72];

#endif
