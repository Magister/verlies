//   Verlies - ein Adventure/RPG im Retrodesign
//
//   Copyright (C) 2018-2023 Heiko Wolf
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License As published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
//   GNU General Public License For more details.
//
//   You should have received a copy of the GNU General Public License along
//   With this program; if not, write to the Free Software Foundation, Inc.,
//   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//   Kontakt: projekte@kabelmail.net


//#pragma bank=12

#include "runen.h"

#include "engine.h"
#include "locations.h"
#include "hud.h"

#include "daten/tilesets/tilesets.h"
#include "daten/txt/locations/locations.h"
#include "daten/lvl/lvldatrotgebirge.h"
#include "daten/lvl/lvldatwiesen.h"
#include "daten/lvl/lvldateichenwald.h"
#include "daten/lvl/lvldatfelsengrund.h"
#include "daten/lvl/lvldatdorfseefeen.h"
#include "daten/lvl/lvldatgipfelpfad.h"

void p_portalrune (void) __banked
{
	if ((v_sstaub != 0) && (v_keyflag == 2)) {
		//von Stadtgefängnis nach Felsengrund
		if (v_region == 2) {
			p_engine_loadTileset (BANK_2, 38, 81, grundtiles, BANK_12);
        		p_engine_loadTileset (BANK_2, 82, 127, font, BANK_12); 
			p_engine_loadTileset (BANK_2, 8, 29, felsengrund_1, BANK_12);
			v_keyflag = 1;
			p_engine_loadMap (v_lvl1a, BANK_5, BANK_12);
                	p_engine_changeLvl (1, 120, 32);
                	p_gui_show_location (lfelsengrund);
			v_kampf = TRUE;
			v_region = 1;
		}
		//von Ogerhöhlen nach Die großen Wiesen
		else if (v_region == 4) {
			p_engine_loadTileset (BANK_2, 38, 81, grundtiles, BANK_12);
        		p_engine_loadTileset (BANK_2, 82, 127, font, BANK_12); 
			p_engine_loadTileset (BANK_2, 3, 34, wiesen_1, BANK_12);
			v_keyflag = 1;
			p_engine_loadMap (v_lvl37, BANK_5, BANK_12);
			p_engine_changeLvl (37, 32, 72);
			p_gui_show_location (lwiesen);
			v_kampf = TRUE;
			v_region = 3;
		}
		//von Feensee nach Seefeendorf
		else if (v_region == 6) {
			p_engine_loadTileset (BANK_2, 38, 81, grundtiles, BANK_12);
        		p_engine_loadTileset (BANK_2, 82, 127, font, BANK_12); 
			p_engine_loadTileset (BANK_2, 3, 34, wiesen_1, BANK_12);
			v_keyflag = 1;
			p_engine_loadMap (v_lvl91, BANK_6, BANK_12);
                	p_engine_changeLvl (91, 104, 104);
			p_gui_show_location (lseefeendorf);
			v_kampf = TRUE;
			v_region = 5;
		}		
		//von Mine nach Rotgebirge
		else if (v_region == 12) {
			p_engine_loadTileset (BANK_2, 38, 81, grundtiles, BANK_12);
        		p_engine_loadTileset (BANK_2, 82, 127, font, BANK_12); 
			p_engine_loadTileset (BANK_2, 3, 34, gebirgspfad, BANK_12);
			v_keyflag = 1;
			p_engine_loadMap (v_lvl198, BANK_19, BANK_12);
			p_engine_changeLvl (198, 80, 40);
			p_gui_show_location (lrotgebirge);
			v_kampf = TRUE;
			v_region = 11;

		}
		//von der Alte Baum nach Eichenwald
		else if (v_region == 8) {
			p_engine_loadTileset (BANK_2, 38, 81, grundtiles, BANK_12);
        		p_engine_loadTileset (BANK_2, 82, 127, font, BANK_12);
        		p_engine_loadTileset (BANK_2, 4, 38, eichenwald, BANK_12);
			v_keyflag = 1;
			p_engine_loadMap (v_lvl142, BANK_18, BANK_12);
			p_engine_changeLvl (142, 128, 72);
			p_gui_show_location (leichenwald);
			v_kampf = TRUE;
			v_region = 7;        		
		}
		//von Phurs Tempel nach Gipfelpfad
		else if (v_region == 14) {
			p_engine_loadTileset (BANK_2, 38, 81, grundtiles, BANK_12);
        		p_engine_loadTileset (BANK_2, 82, 127, font, BANK_12);
        		p_engine_loadTileset (BANK_2, 3, 34, gebirgspfad, BANK_12);
			v_keyflag = 1;
                	p_engine_loadMap (v_lvl241, BANK_19, BANK_12);
                	p_engine_changeLvl (241, 80, 40);
                	p_gui_show_location (lgipfelpfad);
			v_kampf = TRUE;
			v_region = 13;        		
		}
		else if (v_lvl == 300) {
			v_keyflag = 1;
			p_engine_loadMap (v_lvl30, BANK_5, BANK_12);
			p_engine_changeLvl (30, 24, 32);
			v_kampf = TRUE;
		}
		else if (v_lvl == 301) {
			v_keyflag = 1;
			p_engine_loadMap (v_lvl82, BANK_5, BANK_12);
			p_engine_changeLvl (82, 24, 32);
			v_kampf = TRUE;	
		}
		--v_sstaub;
		p_hud_showStaub ();
	}
} 
