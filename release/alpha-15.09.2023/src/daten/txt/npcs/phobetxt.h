//   Verlies - ein Adventure im Retrodesign
//
//   Copyright (C) 2018-2023 Heiko Wolf
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License As published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
//   GNU General Public License For more details.
//
//   You should have received a copy of the GNU General Public License along
//   With this program; if not, write to the Free Software Foundation, Inc.,
//   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//   Kontakt: projekte@kabelmail.net

#ifndef __phobetxt1_h_INCLUDE
#define __phobetxt1_h_INCLUDE

extern const unsigned char phobetxt1[72];
extern const unsigned char phobetxt2[72];
extern const unsigned char phobetxt3[72];
extern const unsigned char phobetxt4[72];

extern const unsigned char phobe_endkampf_1[54];
extern const unsigned char phobe_endkampf_2[54];
extern const unsigned char phobe_endkampf_3[54];
extern const unsigned char phobe_endkampf_4[54];
extern const unsigned char phobe_endkampf_5[54];
extern const unsigned char phobe_endkampf_6[54];

#endif
