//   Verlies - ein Adventure im Retrodesign
//
//   Copyright (C) 2018-2022 Heiko Wolf
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License As published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
//   GNU General Public License For more details.
//
//   You should have received a copy of the GNU General Public License along
//   With this program; if not, write to the Free Software Foundation, Inc.,
//   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//   Kontakt: projekte@kabelmail.net

#ifndef _TRUHENTXT_H_
#define _TRUHENTXT_H_

extern const unsigned char truheleer [72];
extern const unsigned char tschluessel [72];
extern const unsigned char aspektberg[72];
extern const unsigned char aspektwald[72];
extern const unsigned char ausdauertrank[72];
extern const unsigned char brotfach[72];
extern const unsigned char brotbuechse_1[72];
extern const unsigned char brotbuechse_2[72];
extern const unsigned char erhalten[72];
extern const unsigned char erz[72];
extern const unsigned char geldkatze1[72];
extern const unsigned char geldkatze2[72];
extern const unsigned char geldfach[72];
extern const unsigned char goldvoll[72];
extern const unsigned char heiltrank[72];
extern const unsigned char kartetruhe[72];
extern const unsigned char stoff[72];
extern const unsigned char proviant[72];
extern const unsigned char portalrune[72];
extern const unsigned char wappenrock[72];
extern const unsigned char mstone[72];
extern const unsigned char kraeuterbeutel[72];
extern const unsigned char schaufel[72];
extern const unsigned char schwerttruhe[72];
extern const unsigned char zauberstaub[72];
extern const unsigned char portalrune[72];

#endif
