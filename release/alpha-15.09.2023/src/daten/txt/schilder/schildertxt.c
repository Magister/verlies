//   Verlies - ein Adventure im Retrodesign
//
//   Copyright (C) 2018-2023 Heiko Wolf
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License As published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY Or FITNESS For A PARTICULAR PURPOSE.  See the
//   GNU General Public License For more details.
//
//   You should have received a copy of the GNU General Public License along
//   With this program; if not, write to the Free Software Foundation, Inc.,
//   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//   Kontakt: projekte@kabelmail.net

//pragma bank=10

const unsigned char schildtxt1[72] =
{
  0x77,0x7E,0x79,0x7A,0x71,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt1b[72] =
{
  0x78,0x7A,0x79,0x7E,0x7A,0x71,0x78,0x71,0x76,0x6C,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt1c[72] =
{
  0x77,0x7E,0x71,0x7B,0x68,0x7A,0x6D,0x74,0x7A,0x6D,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt1d[72] =
{
  0x6F,0x70,0x6D,0x6B,0x7E,0x73,0x6F,0x73,0x7E,0x6B,
  0x65,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt1e[] =
{
  0x72,0x7E,0x6D,0x74,0x6B,0x69,0x76,0x7A,0x6D,0x6B,
  0x7A,0x73,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};


const unsigned char schildtxt2[72] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x6A,
  0x6A,0x7E,0x6D,0x6D,0x59,0x59,0x59,0x00,0x7D,0x70,
  0x76,0x71,0x78,0x5A,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt3[72] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x65,
  0x6A,0x72,0x00,0x79,0x7A,0x7A,0x71,0x6C,0x7A,0x7A,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x51,0x00
};

const unsigned char schildtxt4[72] =
{
  0x79,0x7A,0x73,0x6C,0x7A,0x71,0x78,0x6D,0x6A,0x71,
  0x7B,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt5[72] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x79,
  0x7A,0x7A,0x71,0x6C,0x7A,0x7A,0x01,0x01,0x01,0x01,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x51,0x00
};

const unsigned char schildtxt6[72] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7A,
  0x76,0x7C,0x77,0x7A,0x71,0x68,0x7E,0x73,0x7B,0x01,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x51,0x00
};

const unsigned char schildtxt7[72] =
{
  0x7B,0x7A,0x6D,0x00,0x7E,0x73,0x6B,0x7A,0x00,0x7D,
  0x7E,0x6A,0x72,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x51
};

const unsigned char schildtxt8[72] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x65,
  0x6A,0x72,0x00,0x7A,0x73,0x79,0x7A,0x71,0x77,0x7E,
  0x76,0x71,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x51,0x00
};

const unsigned char schildtxt9[72] =
{
  0x78,0x7A,0x7D,0x76,0x6D,0x78,0x6C,0x6F,0x79,0x7E,
  0x7B,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x65,0x6A,0x6D,0x00,
  0x65,0x68,0x7A,0x6D,0x78,0x7A,0x71,0x72,0x76,0x7A,
  0x71,0x7A,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt10[] =
{
  0x77,0x7A,0x76,0x72,0x00,0x7B,0x7A,0x6D,0x00,0x65,
  0x68,0x7A,0x6D,0x78,0x7A,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x6C,0x76,0x71,0x7B,
  0x00,0x72,0x6A,0x7A,0x6D,0x6D,0x76,0x6C,0x7C,0x77,
  0x5A,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt11[72] =
{
  0x6D,0x70,0x6B,0x78,0x7A,0x7D,0x76,0x6D,0x78,0x7A,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x65,0x6A,0x6D,0x00,
  0x72,0x76,0x71,0x7A,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt12[72] =
{
  0x7B,0x76,0x7A,0x00,0x72,0x76,0x71,0x7A,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x7F,0x51
};

const unsigned char schildtxt13[72] =
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x77,
  0x7A,0x73,0x72,0x7F,0x7E,0x6A,0x79,0x5A,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7F,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x51,0x00
};

const unsigned char schildtxt14[72] =
{
  0x72,0x76,0x71,0x7A,0x71,0x7D,0x7E,0x77,0x71,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x7E,0x6A,0x6C,0x6C,
  0x7A,0x6D,0x00,0x7D,0x7A,0x6B,0x6D,0x76,0x7A,0x7D,
  0x5A,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x51,0x00
};

const unsigned char schildtxt15[72] =
{
  0x6C,0x7C,0x77,0x7E,0x6B,0x65,0x74,0x7E,0x72,0x72,
  0x7A,0x6D,0x7F,0x00,0x00,0x00,0x00,0x00,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0x6F,0x79,0x70,0x6B,
  0x7A,0x71,0x7F,0x68,0x7A,0x78,0x58,0x6C,0x70,0x71,
  0x6C,0x6B,0x00,0x00,0x6F,0x79,0x70,0x6B,0x7A,0x00,
  0x7E,0x7D,0x5A,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x51,0x00
};

//Raum 241 (Gipfelpfad) 
const unsigned char schildtxt16[] =
{
  0x6F,0x77,0x6A,0x6D,0x6C,0x7F,0x6B,0x7A,0x72,0x6F,
  0x7A,0x73,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt17[72] =
{
  0x6F,0x70,0x6D,0x6B,0x7E,0x73,0x6D,0x7E,0x6A,0x72,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt18[72] =
{
  0x79,0x7A,0x73,0x6C,0x7A,0x71,0x78,0x6D,0x6A,0x71,
  0x7B,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt19[72] =
{
  0x7B,0x70,0x6D,0x79,0x7F,0x7B,0x7A,0x6D,0x7F,0x6C,
  0x7A,0x7A,0x79,0x7A,0x7A,0x71,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt20[72] =
{
  0x7A,0x73,0x79,0x7A,0x71,0x77,0x7E,0x76,0x71,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt21[72] =
{
  0x65,0x68,0x7A,0x6D,0x78,0x7A,0x71,0x77,0x7A,0x76,
  0x72,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt22[72] =
{
  0x71,0x70,0x6D,0x7B,0x56,0x00,0x70,0x78,0x7A,0x6D,
  0x77,0x70,0x7A,0x77,0x73,0x7A,0x71,0x7F,0x70,0x6C,
  0x6B,0x00,0x56,0x7F,0x79,0x7A,0x73,0x6C,0x7A,0x71,
  0x78,0x6D,0x6A,0x71,0x7B,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt23[72] =
{
  0x68,0x7A,0x6C,0x6B,0x56,0x7F,0x70,0x78,0x7A,0x6D,
  0x77,0x70,0x7A,0x77,0x73,0x7A,0x7F,0x7F,0x70,0x6C,
  0x6B,0x56,0x7F,0x7F,0x79,0x7A,0x73,0x6C,0x7A,0x71,
  0x78,0x6D,0x6A,0x71,0x7B,0x7F,0x6C,0x6A,0x7A,0x7B,
  0x56,0x7F,0x6C,0x7A,0x7A,0x79,0x7A,0x7A,0x71,0x7B,
  0x70,0x6D,0x79,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt24[72] =
{
  0x65,0x6A,0x72,0x7F,0x7A,0x76,0x7C,0x77,0x7A,0x71,
  0x68,0x7E,0x73,0x7B,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};

const unsigned char schildtxt25[72] =
{
  0x69,0x70,0x6D,0x6C,0x76,0x7C,0x77,0x6B,0x58,0x76,
  0x71,0x7F,0x7B,0x76,0x7A,0x6C,0x7A,0x6D,0x6D,0x76,
  0x7C,0x77,0x6B,0x6A,0x71,0x78,0x7F,0x73,0x7A,0x7D,
  0x7A,0x71,0x7F,0x76,0x6D,0x53,0x78,0x7A,0x71,0x7B,
  0x68,0x70,0x7F,0x72,0x6A,0x7A,0x6D,0x6D,0x76,0x6C,
  0x7C,0x77,0x7A,0x7F,0x65,0x68,0x7A,0x6D,0x78,0x7A,
  0x5A,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,
  0x7F,0x51
};
