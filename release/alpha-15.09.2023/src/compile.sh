#https://github.com/gbdk-2020/gbdk-2020/releases/download/4.1.1/gbdk-linux64.tar.gz

#!/bin/bash

echo -e "Start compiling...\n\n"

date

time make

date

echo -e "\nReady. Hope to start the Game...\n"

if [ -f obj/verlies.gb ]
then

		
	#https://github.com/bbbbbr/romusage
	romusage obj/verlies.map -sRe -sRp
	
	echo "Starte Emulation"

	cp obj/verlies.gb bin/ 
	
	#https://emulicious.net/	
	emulicious.sh bin/verlies.gb
else

	echo -e "No gb file for start!\n"
	exit 1

fi

exit 0

#2023, projekte@kabelmail.net
